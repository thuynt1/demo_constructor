package demo;

public class NhanVien extends ConNguoi implements HoatDongTieuCuc{
	private String manhanvien;
	private String phongban;
	private double mucluong;
	
	public NhanVien() {
	};


	public NhanVien(String manhanvien, String phongban, double mucluong) {
		this.manhanvien = manhanvien;
		this.phongban = phongban;
		this.mucluong = mucluong;
	}

	@Override
	public String toString() {
		return "NhanVien [manhanvien=" + manhanvien + ", phongban=" + phongban + ", mucluong=" + mucluong
				+ ", getTen()=" + getTen() + ", getNgaysinh()=" + getNgaysinh() + ", getGioitinh()=" + getGioitinh()
				+ ", getDiachi()=" + getDiachi() + "]";
	}

	public String getManhanvien() {
		return manhanvien;
	}



	public void setManhanvien(String manhanvien) {
		this.manhanvien = manhanvien;
	}



	public String getPhongban() {
		return phongban;
	}



	public void setPhongban(String phongban) {
		this.phongban = phongban;
	}



	public double getMucluong() {
		return mucluong;
	}



	public void setMucluong(double mucluong) {
		this.mucluong = mucluong;
	}


	public void gioithieu() {
		System.out.println("Toi la : " + this.getTen() + "toi la nhan vien van phong " + this.phongban + "voi ma so la " + this.manhanvien);
	};

	@Override
	public void sudungMayTinh() {
		System.out.println("Nhan vien su dung may tinh");
	}

	@Override
	public void choicobac() {
		System.out.println("Nhan Vien choi co bac");
	}

	@Override
	public void uongruou() {
		System.out.println("Nhan Vien choi uong ruou");
	}

	@Override
	public void hutthuoc() {
		System.out.println("Nhan Vien hut thuoc");
	}
	
}
