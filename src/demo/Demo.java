package demo;

public class Demo {

	public static void main(String[] args) {
		SinhVien sv1 = new SinhVien();
		sv1.setDiachi("HN");
		sv1.setEmail("sv1@gmail.com");
		sv1.setGioitinh(1);
		sv1.setLop("d09vt1");
		sv1.setMasinhvien("1q2w3e");
		sv1.setNgaysinh("20-12-1998");
		sv1.setTen("Nguyen Thi Huong");
		
		sv1.an();
		sv1.chay();
		sv1.choi();
		sv1.docsach();
		sv1.getDiachi();
		sv1.getEmail();
		sv1.getLop();
		sv1.getMasinhvien();
		sv1.getNgaysinh();
		sv1.getTen();
		sv1.gioithieu();
		sv1.nghenhac();
		sv1.ngu();
		sv1.sudungMayTinh();
		sv1.luyentapthethao();
		System.out.println(sv1.toString());
		
		SinhVien sv2 = new SinhVien("D10", "vt2", "thuynt1@nal.vn");
		sv2.an();
		sv2.chay();
		sv2.choi();
		sv2.docsach();
		sv2.getDiachi();
		sv2.getEmail();
		sv2.getLop();
		sv2.getMasinhvien();
		sv2.getNgaysinh();
		sv2.getTen();
		sv2.gioithieu();
		sv2.nghenhac();
		sv2.ngu();
		sv2.sudungMayTinh();
		sv2.luyentapthethao();
		System.out.println(sv1.toString());
		
		NhanVien nv1 = new NhanVien();
		nv1.setDiachi("HD");
		nv1.setGioitinh(2);
		nv1.setNgaysinh("2017-01-01");
		nv1.setTen("Thuy");
		
		nv1.an();
		nv1.chay();
		nv1.choi();
		nv1.choicobac();
		nv1.getDiachi();
		nv1.getGioitinh();
		nv1.getNgaysinh();
		nv1.getTen();
		System.out.println(nv1.toString());
		
		NhanVien nv2 = new NhanVien("D1","ke toan", 1000);
		nv2.an();
		nv2.chay();
		nv2.choi();
		nv2.choicobac();
		nv2.setDiachi("HN");
		nv2.setGioitinh(2);
		nv2.getDiachi();
		nv2.getGioitinh();
	}

}
