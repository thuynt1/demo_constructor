package demo;

public abstract class ConNguoi {

	private String ten;
	private String ngaysinh;
	private int gioitinh;
	private String diachi;

	public void an() {
		System.out.println("method an");
	};
	
	public void ngu() {
		System.out.println("method ngu");
	};
	
	public void chay() {
		System.out.println("method chay");
	};
	
	public void choi() {
		System.out.println("method choi");
	};
	
	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getNgaysinh() {
		return ngaysinh;
	}

	public void setNgaysinh(String ngaysinh) {
		this.ngaysinh = ngaysinh;
	}

	public int getGioitinh() {
		return gioitinh;
	}

	public void setGioitinh(int gioitinh) {
		this.gioitinh = gioitinh;
	}

	public String getDiachi() {
		return diachi;
	}

	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}

	public void gioithieu() {
		System.out.println("method gioi thieu");
	};
	
	public abstract void sudungMayTinh();
}
