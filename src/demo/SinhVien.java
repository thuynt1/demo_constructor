package demo;

public class SinhVien extends ConNguoi implements HoatDongTichCuc {
	
	private String masinhvien;
	private String lop;
	private String email;
	
	public SinhVien() {
	};

	public SinhVien(String masinhvien, String lop, String email) {
		this.masinhvien = masinhvien;
		this.lop = lop;
		this.email = email;
	};
	
	@Override
	public String toString() {
		return "SinhVien [masinhvien=" + masinhvien + ", lop=" + lop + ", email=" + email + ", getTen()=" + getTen()
				+ ", getNgaysinh()=" + getNgaysinh() + ", getGioitinh()=" + getGioitinh() + ", getDiachi()="
				+ getDiachi() + "]";
	}

	public void sudungMayTinh(){
		System.out.println("Su dung may tinh");
	};
	@Override
	public void docsach() {
		System.out.println("sinh vien doc sach");
	}
	@Override
	public void luyentapthethao() {
		System.out.println("sinh vien luyen tap the thao");
	}
	@Override
	public void nghenhac() {
		System.out.println("sinh vien nghe nhac");
	}
	public String getMasinhvien() {
		return masinhvien;
	}

	public void setMasinhvien(String masinhvien) {
		this.masinhvien = masinhvien;
	}

	public String getLop() {
		return lop;
	}

	public void setLop(String lop) {
		this.lop = lop;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public void uongCapheSang( ) {
		System.out.println("sinh vien uong cafe sang");
	};
	
	public void gioithieu() {
		System.out.println("Toi la : " + this.getTen() + "toi la sinh vien voi ma so la" + this.masinhvien);
	};
	
	}
